// MAP INVERTER
// CODE BY PLAGMAN, with additions by DeeperThought and later changes by NY00123

/* These values are used as short-term temporaries (e.g., function arguments),
   and thus aren't attached to multiplayer packets.
   The prefix mapmirroringmod is used in order to reduce the chances of symbol
   collisions with other mods. It can be seen as a kind of a namespace.  */

// Function arguments
gamevar mapmirroringmod_A0 0 524288
gamevar mapmirroringmod_A1 0 524288
// Temporaries that shall not be modified due to a state call.
// L0 and L1 are used here as loop vars.
gamevar mapmirroringmod_C 0 524288
gamevar mapmirroringmod_L0 0 524288
gamevar mapmirroringmod_L1 0 524288
// Other temporaries that states may freely modify.
gamevar mapmirroringmod_S0 0 524288
gamevar mapmirroringmod_S1 0 524288
gamevar mapmirroringmod_S2 0 524288
gamevar mapmirroringmod_S3 0 524288

gamevar mapmirroringmod_HasPlyr0 0 0

gamearray mapmirroringmod_SwpWallAr 16384

gamevar mapmirroringmod_InitCstat -1 2

// This helper function returns 1 if a wall's picnum is identified
// as a wall switch. Otherwise, it returns 0. Input gamevar isn't changed.
state mapmirroringmod_iswallswitch
    switch mapmirroringmod_A0
        case 130 // ACCESSSWITCH
        case 131 // ACCESSSWITCH+1; Not usable, but checked for consistency
        case 132 // SLOTDOOR
        case 133 // SLOTDOOR+1
        case 134 // LIGHTSWITCH
        case 135 // LIGHTSWITCH+1
        case 136 // SPACEDOORSWITCH
        case 137 // SPACEDOORSWITCH+1
        case 138 // SPACELIGHTSWITCH
        case 139 // SPACELIGHTSWITCH+1
        case 140 // FRANKENSTINESWITCH
        case 141 // FRANKENSTINESWITCH+1
        case 146 // MULTISWITCH
        case 147 // MULTISWITCH+1
        case 148 // MULTISWITCH+2
        case 149 // MULTISWITCH+3
        case 162 // DIPSWITCH
        case 163 // DIPSWITCH+1
        case 164 // DIPSWITCH2
        case 165 // DIPSWITCH2+1
        case 166 // TECHSWITCH
        case 167 // TECHSWITCH+1
        case 168 // DIPSWITCH3
        case 169 // DIPSWITCH3+1
        case 170 // ACCESSSWITCH2
        case 171 // ACCESSSWITCH2+1; Not usable, but checked for consistency
        case 712 // LIGHTSWITCH2
        case 713 // LIGHTSWITCH2+1
        case 860 // POWERSWITCH1
        case 861 // POWERSWITCH1+1
        case 862 // LOCKSWITCH1
        case 863 // LOCKSWITCH1+1
        case 864 // POWERSWITCH2
        case 865 // POWERSWITCH2+1
        case 1155 // HANDPRINTSWITCH
//      case 1156 // Unrelated to HANDPRINTSWITCH, but checked in Duke3D's code
        case 1142 // ALIENSWITCH
        case 1143 // ALIENSWITCH+1
        case 1122 // PULLSWITCH
        case 1123 // PULLSWITCH+1
        case 1111 // HANDSWITCH
        case 1112 // HANDSWITCH+1
            setvar RETURN 1
            break
        default
            setvar RETURN 0
            break
    endswitch
ends

// This function swaps the data of walls mapmirroringmod_A0
// and mapmirroringmod_A1, except for the point2 fields.
// None of the input gamevars is changed.
state mapmirroringmod_swapwall
ifvarvare mapmirroringmod_A0 mapmirroringmod_A1
    break
getwall[mapmirroringmod_A0].x mapmirroringmod_S0
getwall[mapmirroringmod_A1].x mapmirroringmod_S1
setwall[mapmirroringmod_A0].x mapmirroringmod_S1
setwall[mapmirroringmod_A1].x mapmirroringmod_S0
getwall[mapmirroringmod_A0].y mapmirroringmod_S0
getwall[mapmirroringmod_A1].y mapmirroringmod_S1
setwall[mapmirroringmod_A0].y mapmirroringmod_S1
setwall[mapmirroringmod_A1].y mapmirroringmod_S0
getwall[mapmirroringmod_A0].cstat mapmirroringmod_S0
getwall[mapmirroringmod_A1].cstat mapmirroringmod_S1
setwall[mapmirroringmod_A0].cstat mapmirroringmod_S1
setwall[mapmirroringmod_A1].cstat mapmirroringmod_S0
getwall[mapmirroringmod_A0].picnum mapmirroringmod_S0
getwall[mapmirroringmod_A1].picnum mapmirroringmod_S1
setwall[mapmirroringmod_A0].picnum mapmirroringmod_S1
setwall[mapmirroringmod_A1].picnum mapmirroringmod_S0
getwall[mapmirroringmod_A0].overpicnum mapmirroringmod_S0
getwall[mapmirroringmod_A1].overpicnum mapmirroringmod_S1
setwall[mapmirroringmod_A0].overpicnum mapmirroringmod_S1
setwall[mapmirroringmod_A1].overpicnum mapmirroringmod_S0
getwall[mapmirroringmod_A0].shade mapmirroringmod_S0
getwall[mapmirroringmod_A1].shade mapmirroringmod_S1
setwall[mapmirroringmod_A0].shade mapmirroringmod_S1
setwall[mapmirroringmod_A1].shade mapmirroringmod_S0
getwall[mapmirroringmod_A0].pal mapmirroringmod_S0
getwall[mapmirroringmod_A1].pal mapmirroringmod_S1
setwall[mapmirroringmod_A0].pal mapmirroringmod_S1
setwall[mapmirroringmod_A1].pal mapmirroringmod_S0
getwall[mapmirroringmod_A0].xrepeat mapmirroringmod_S0
getwall[mapmirroringmod_A1].xrepeat mapmirroringmod_S1
setwall[mapmirroringmod_A0].xrepeat mapmirroringmod_S1
setwall[mapmirroringmod_A1].xrepeat mapmirroringmod_S0
getwall[mapmirroringmod_A0].yrepeat mapmirroringmod_S0
getwall[mapmirroringmod_A1].yrepeat mapmirroringmod_S1
setwall[mapmirroringmod_A0].yrepeat mapmirroringmod_S1
setwall[mapmirroringmod_A1].yrepeat mapmirroringmod_S0
getwall[mapmirroringmod_A0].xpanning mapmirroringmod_S0
getwall[mapmirroringmod_A1].xpanning mapmirroringmod_S1
setwall[mapmirroringmod_A0].xpanning mapmirroringmod_S1
setwall[mapmirroringmod_A1].xpanning mapmirroringmod_S0
getwall[mapmirroringmod_A0].ypanning mapmirroringmod_S0
getwall[mapmirroringmod_A1].ypanning mapmirroringmod_S1
setwall[mapmirroringmod_A0].ypanning mapmirroringmod_S1
setwall[mapmirroringmod_A1].ypanning mapmirroringmod_S0
getwall[mapmirroringmod_A0].lotag mapmirroringmod_S0
getwall[mapmirroringmod_A1].lotag mapmirroringmod_S1
setwall[mapmirroringmod_A0].lotag mapmirroringmod_S1
setwall[mapmirroringmod_A1].lotag mapmirroringmod_S0
getwall[mapmirroringmod_A0].hitag mapmirroringmod_S0
getwall[mapmirroringmod_A1].hitag mapmirroringmod_S1
setwall[mapmirroringmod_A0].hitag mapmirroringmod_S1
setwall[mapmirroringmod_A1].hitag mapmirroringmod_S0
getwall[mapmirroringmod_A0].extra mapmirroringmod_S0
getwall[mapmirroringmod_A1].extra mapmirroringmod_S1
setwall[mapmirroringmod_A0].extra mapmirroringmod_S1
setwall[mapmirroringmod_A1].extra mapmirroringmod_S0
getwall[mapmirroringmod_A0].nextwall mapmirroringmod_S0
getwall[mapmirroringmod_A1].nextwall mapmirroringmod_S1
setwall[mapmirroringmod_A0].nextwall mapmirroringmod_S1
setwall[mapmirroringmod_A1].nextwall mapmirroringmod_S0
getwall[mapmirroringmod_A0].nextsector mapmirroringmod_S0
getwall[mapmirroringmod_A1].nextsector mapmirroringmod_S1
setwall[mapmirroringmod_A0].nextsector mapmirroringmod_S1
setwall[mapmirroringmod_A1].nextsector mapmirroringmod_S0
ends

onevent EVENT_PRELEVEL
setvar mapmirroringmod_HasPlyr0 0

// Adjust sector ceilings and floors
setvar mapmirroringmod_L0 0
whilevarvarn mapmirroringmod_L0 NUMSECTORS
{
    // If bit 2 (swap x&y) is set, invert bit 5 (y-flip).
    // Otherwise, invert bit 4 (x-flip).

    getsector[mapmirroringmod_L0].ceilingstat mapmirroringmod_S0
    ifvarand mapmirroringmod_S0 4
        xorvar mapmirroringmod_S0 32
    else
        xorvar mapmirroringmod_S0 16
    setsector[mapmirroringmod_L0].ceilingstat mapmirroringmod_S0

    getsector[mapmirroringmod_L0].floorstat mapmirroringmod_S0
    ifvarand mapmirroringmod_S0 4
        xorvar mapmirroringmod_S0 32
    else
        xorvar mapmirroringmod_S0 16
    setsector[mapmirroringmod_L0].floorstat mapmirroringmod_S0

    addvar mapmirroringmod_L0 1
}

// Horizontally flip the walls
setvar mapmirroringmod_L0 0
whilevarvarn mapmirroringmod_L0 NUMWALLS
{
    // Flip each wall point's x coordinate. This will temporarily make
    // the map technically invalid, because a sector's exterior walls
    // (e.g., all walls of a convex sector) will be sorted in
    // anti-clockwise order, instead of clockwise. This is fixed later.
    getwall[mapmirroringmod_L0].x mapmirroringmod_S0
    mulvar mapmirroringmod_S0 -1
    setwall[mapmirroringmod_L0].x mapmirroringmod_S0
    // Initialize mapmirroringmod_SwpWallAr for later use
    setarray mapmirroringmod_SwpWallAr[mapmirroringmod_L0] -1
    // Visually flip a wall's tile, except for specific tile numbers
    getwall[mapmirroringmod_L0].picnum mapmirroringmod_A0
    switch mapmirroringmod_A0
        case 277 case 394 case 415 case 498 case 499
        case 500 case 502 case 518 case 519 case 520 case 521 case 522 case 523 case 524 case 525
        case 526 case 527 case 528 case 529 case 530 case 531 case 532 case 533 case 534 case 535
        case 568 case 593 case 594 case 598 case 599 case 600 case 612 case 613 case 614 case 639
        case 657 case 666 case 667 case 687 case 699 case 721 case 728 case 736 case 766 case 776 
        case 777 case 778 case 784 case 785 case 817 case 818 case 822 case 826 case 835 case 838 case 837
        case 841 case 839 case 848 case 849 case 856 case 858 case 871 case 872 case 937 case 938 
        case 945 case 946 case 949 case 961 case 963 case 964 case 965 case 966 case 967 case 968
        case 970 case 980 case 992 case 1043 case 1044 case 1046 case 1047 case 1064 case 1065 case 1066
        case 1072 case 1210 case 1211 case 1212 case 1214 case 1215 case 1217 case 1244 case 1245
        case 2486 case 2492 case 2493 case 2500 case 2504 case 2505 case 2506 case 3370 case 3371 case 3372
        case 3373 case 3374 case 3382 case 3383 case 3391 case 3402 case 3403 case 3421 case 3422 case 3423
        case 4096 case 4108 case 4125 case 4126 case 4238 case 4306 case 4307 case 4318 case 4323
        case 4329 case 4447 case 4450 case 4451 case 4477 case 4472 case 4475 case 4476
        case 4478 case 4479 case 4484 case 4485 case 4486 case 4487 case 4488 case 4492 case 4499
        case 4494 case 4504 case 4505 case 4506 case 4507 case 4508 case 4553 case 4564 case 4565
        case 4578 case 4579 case 4597 case 4598 case 4599 case 4889 case 4891 case 4892 case 4896 
        case 4897 case 4898 case 4899 case 4903 case 4904 case 4905 case 4906 case 4907 case 4908
        case 4924 case 4925 case 4926 case 4927 case 4928 case 4929 case 4930 case 4931 case 4932
        case 4933 case 4934 case 4937 case 4938 case 4939 case 4942 case 4945 case 4947 case 4948
        case 4950 case 4951 case 4952 case 4956
            break
        default
            state mapmirroringmod_iswallswitch
            ifvarn RETURN 0 break

            getwall[mapmirroringmod_L0].cstat mapmirroringmod_S0
            xorvar mapmirroringmod_S0 8
            setwall[mapmirroringmod_L0].cstat mapmirroringmod_S0
            break
    endswitch
    addvar mapmirroringmod_L0 1
}

// Make exterior walls clockwise-ordered again, one cycle after the other
setvar mapmirroringmod_L0 0
whilevarvarn mapmirroringmod_L0 NUMWALLS
{
    // A cycle's first and last wall numbers
    // will be mapmirroringmod_S0 and mapmirroringmod_A0, respectively.
    setvarvar mapmirroringmod_S0 mapmirroringmod_L0
    getwall[mapmirroringmod_L0].point2 mapmirroringmod_S1
    setvarvar mapmirroringmod_A0 mapmirroringmod_L0
    whilevarvarn mapmirroringmod_S1 mapmirroringmod_S0
    {
        addvar mapmirroringmod_A0 1
        getwall[mapmirroringmod_A0].point2 mapmirroringmod_S1
    }
    // Swap walls mapmirroringmod_L0 and mapmirroringmod_C (mapmirroringmod_A0),
    // then mapmirroringmod_L0+1 and mapmirroringmod_C-1, and so on.
    setvarvar mapmirroringmod_C mapmirroringmod_A0
    setvarvar mapmirroringmod_A1 mapmirroringmod_L0
    setvarvar mapmirroringmod_L1 mapmirroringmod_A0
    subvarvar mapmirroringmod_L1 mapmirroringmod_A1
    divvar mapmirroringmod_L1 2
    addvar mapmirroringmod_A1 1
    whilevarn mapmirroringmod_L1 0
    {
        state mapmirroringmod_swapwall
        setarray mapmirroringmod_SwpWallAr[mapmirroringmod_A1] mapmirroringmod_A0
        setarray mapmirroringmod_SwpWallAr[mapmirroringmod_A0] mapmirroringmod_A1
        addvar mapmirroringmod_A1 1
        addvar mapmirroringmod_A0 -1
        addvar mapmirroringmod_L1 -1
    }

    // Repeatedly swap the x,y fields of walls mapmirroringmod_A1 (first wall)
    // and each other wall in the cycle, in the given order.
    setvarvar mapmirroringmod_A1 mapmirroringmod_L0
    whilevarvarn mapmirroringmod_L0 mapmirroringmod_C
    {
        setvarvar mapmirroringmod_A0 mapmirroringmod_L0
        addvar mapmirroringmod_A0 1
        getwall[mapmirroringmod_A0].x mapmirroringmod_S0
        getwall[mapmirroringmod_A1].x mapmirroringmod_S1
        setwall[mapmirroringmod_A0].x mapmirroringmod_S1
        setwall[mapmirroringmod_A1].x mapmirroringmod_S0
        getwall[mapmirroringmod_A0].y mapmirroringmod_S0
        getwall[mapmirroringmod_A1].y mapmirroringmod_S1
        setwall[mapmirroringmod_A0].y mapmirroringmod_S1
        setwall[mapmirroringmod_A1].y mapmirroringmod_S0

        addvar mapmirroringmod_L0 1
    }

    // Continue to the next cycle
    addvar mapmirroringmod_L0 1
}

// Fix the nextwall fields by using mapmirroringmod_SwpWallAr
setvar mapmirroringmod_L0 0
whilevarvarn mapmirroringmod_L0 NUMWALLS
{
    getwall[mapmirroringmod_L0].nextwall mapmirroringmod_S0
    ifvarn mapmirroringmod_S0 -1
        ifvarn mapmirroringmod_SwpWallAr[mapmirroringmod_S0] -1
            setwall[mapmirroringmod_L0].nextwall mapmirroringmod_SwpWallAr[mapmirroringmod_S0]

    addvar mapmirroringmod_L0 1
}

// Process actors/sprites. cstat isn't changed here.
setvar mapmirroringmod_L0 0
whilevarn mapmirroringmod_L0 16384
{
    // Flip the x coordinate
    getactor[mapmirroringmod_L0].x mapmirroringmod_S0
    mulvar mapmirroringmod_S0 -1
    setactor[mapmirroringmod_L0].x mapmirroringmod_S0
    
    // Flip the angle
    getactor[mapmirroringmod_L0].ang mapmirroringmod_S0
    ifvarg mapmirroringmod_S0 1024
    {
        setvar mapmirroringmod_S1 1536
    } else {
        setvar mapmirroringmod_S1 512
    }
    subvarvar mapmirroringmod_S1 mapmirroringmod_S0
    mulvar mapmirroringmod_S1 2
    addvarvar mapmirroringmod_S0 mapmirroringmod_S1

    setactor[mapmirroringmod_L0].ang mapmirroringmod_S0

    // Adjust sector effectors
    getactor[mapmirroringmod_L0].picnum mapmirroringmod_A0
    getactor[mapmirroringmod_L0].lotag mapmirroringmod_A1
    ifvare mapmirroringmod_A0 SECTOREFFECTOR ifvare mapmirroringmod_A1 0
    {
        getactor[mapmirroringmod_L0].pal mapmirroringmod_S1
        ifvare mapmirroringmod_S1 2
            setactor[mapmirroringmod_L0].pal 0
        else ifvare mapmirroringmod_S1 0
            setactor[mapmirroringmod_L0].pal 2
    }
    ifvare mapmirroringmod_A0 SECTOREFFECTOR ifvare mapmirroringmod_A1 1
    {
        ifvare mapmirroringmod_S0 512
            setactor[mapmirroringmod_L0].ang 1536
        else
            setactor[mapmirroringmod_L0].ang 512
    }
    ifvare mapmirroringmod_A0 SECTOREFFECTOR ifvare mapmirroringmod_A1 11
    {
        ifvarg mapmirroringmod_S0 0 ifvarl mapmirroringmod_S0 1025
            subvar mapmirroringmod_S0 1
        else
            ifvarg mapmirroringmod_S0 1024 ifvarl mapmirroringmod_S0 2047
                addvar mapmirroringmod_S0 1
        addvar mapmirroringmod_S0 1025
        ifvarg mapmirroringmod_S0 2047 subvar mapmirroringmod_S0 2048
        setactor[mapmirroringmod_L0].ang mapmirroringmod_S0
    }

    addvar mapmirroringmod_L0 1
}

endevent // EVENT_PRELEVEL


onevent EVENT_EGS

// We only need to mirror the actor representing player 0
ifactor APLAYER ifvare mapmirroringmod_HasPlyr0 0
{
    // Flip the player's x coordinate
    getactor[THISACTOR].x mapmirroringmod_A0
    mulvar mapmirroringmod_A0 -1
    setactor[THISACTOR].x mapmirroringmod_A0
    getactor[THISACTOR].y mapmirroringmod_A1
    getactor[THISACTOR].z mapmirroringmod_S0
    // Ensure the sector number is correct
    updatesectorz mapmirroringmod_A0 mapmirroringmod_A1 mapmirroringmod_S0 mapmirroringmod_S1
    changespritesect THISACTOR mapmirroringmod_S1

    // Flip the angle
    getactor[THISACTOR].ang mapmirroringmod_S0
    ifvarg mapmirroringmod_S0 1024
    {
        setvar mapmirroringmod_S1 1536
    } else {
        setvar mapmirroringmod_S1 512
    }
    subvarvar mapmirroringmod_S1 mapmirroringmod_S0
    mulvar mapmirroringmod_S1 2
    addvarvar mapmirroringmod_S0 mapmirroringmod_S1
    
    setactor[THISACTOR].ang mapmirroringmod_S0
    setvar mapmirroringmod_HasPlyr0 1
}

endevent

// Adjust a sprite's cstat
state mapmirroringmod_fliplist

getactor[THISACTOR].picnum mapmirroringmod_S0
switch mapmirroringmod_S0
    case 255 case 341 case 387 case 391 case 479 case 487
    case 489 case 515 case 516 case 544 case 546 case 547
    case 548 case 549 case 594 case 631 case 798 case 860 case 861
    case 913 case 918 case 993 case 1009 case 1011 case 1022 case 1049
    case 1050 case 1148 case 1149 case 1150 case 1175 case 1225
    case 1337 case 1338 case 1345 case 1346 case 1356 case 1357 case 2491
    case 3426 case 3427 case 3428 case 4352 case 4364 case 4365
    case 4416 case 4417 case 4423 case 4424 case 4430 case 4431
    case 4432 case 4433 case 4434 case 4435 case 4456 case 4457
    case 4480 case 4481 case 4482 case 4533 case 4558 case 4588
    case 4589 case 4600 case 4601 case 4602 case 4603 case 4604
    case 4605 case 4954
        xorvar mapmirroringmod_InitCstat 4
        setactor[THISACTOR].cstat mapmirroringmod_InitCstat
break

endswitch

ends

onevent EVENT_GAME

ifvare mapmirroringmod_InitCstat -1
{
    getactor[THISACTOR].cstat mapmirroringmod_InitCstat
    state mapmirroringmod_fliplist
}

endevent
